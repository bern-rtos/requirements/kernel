# Changelog

Requirement changes of the Bern RTOS kernel will be tracked in this document. See [Commits in GitLab](https://gitlab.com/bern-rtos/requirements/kernel/-/commits/master) for details.


## v1.2 (2022-08-03)

**New Requirements**

- None

**Modifications**

- Update issue states

## v1.1 (2021-10-27)

**New Requirements**

- None

**Modifications**

- Add changelog
- Update issue states and links


## v1.0 (2021-04-08)

**New Requirements**

- All requirements

**Modifications**

- None