# Introduction

## Purpose

This document specifies and tracks the status of the requirements for all components that make up the Bern RTOS *kernel*. Other parts of the Bern RTOS are not part of this SRS.




## Intended Audience

This document provides the Bern RTOS *maintainer* a complete set of features that are to be implemented tested. *Users* of the Bern RTOS can look-up which features are planned and request new ones. There is a GitLab issue for every functional requirement. Issues are used to discuss the content of a requirement and contain additional information for development.

To request a new requirement you can create an issue [here](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/new?issuable_template=Feature%20Request). To change of the current behavior of the kernel create an enhancement issue [here](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/new?issuable_template=Enhancement). Issues are tracked on GitLab but you can also use your GitHub or BitBucket account to log in.

*Be advised that feature requests might be deferred until the end of Stefan Lüthi (@luethi) master thesis (summer 2022).*

In general this document intends serve as a summary *what* the Bern RTOS can and will be able to do and *why* it is part of the RTOS.


## Product Scope

The kernel is the core part of any RTOS. It schedules tasks, enables communication between them and manages resources. The Bern RTOS kernel aims to make highly concurrent applications safer than a bare-metal implementation.


## Requirement State

A requirement can be in one of the following states:

1. *to review*: The requirement was recently added and needs discussing first.

2. *planned*: The implementation of the requirement is planned within the current project scope.

3. *done*: The requirement is fulfilled (implemented and tested).

4. *invalid*: The requirement is no longer relevant or valid. 


Requirements are grouped by topic and represented with an epic, which can have one of three states:

1. *draft*: All requirements within this epic are to be reviewed.

2. *active*: The epic is active. The requirement state applies.

3. *inactive*: The epic is no longer relevant or valid. All requirement states are invalid.

## Acronyms

|   |   |
|---|---|
| IPC | Inter-Process Communication |
| MPU | Memory Protection Unit |
| ROM | read-only memory |
| RTOS | real-time operating system |
| SRS | software requirements specification |