# F003: Message Queues

|   |   |
|---|---|
| **Epic** | [GitLab Epic](https://gitlab.com/groups/bern-rtos/kernel/-/epics/3) |
| **Document Status** | active |

## Description

Data can be transferred safely between tasks with message queues. 

## Requirements

| ID | Title | User Story | Priority | Status |
|----|-------|------------|----------|--------|
| F003-1 | [Message Queue](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/19) | As a developer, I want to send messages from one task to another, so that I can communicate data between tasks. | high | done | 
| F003-2 | [Message Queue Size](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/20) | As a developer, I want to allocate a queue of fixed size within a task, so that no messages get lost when they're not read immediately. | medium | done | 
| F003-3 | [Message Queue Access](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/21) | As a developer, I want to send messages from multiple tasks to one queue, so that the memory overhead from the queues is minimal. | medium | done | 
| F003-4 | [FIFO Message Queue](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/22) | As a developer, I want the queue to be of FIFO manner, so that the order of messages is preserved. | medium | done | 
| F003-5 | [Reference Messaging]() | As a developer, I want to send data efficiently, so that large data blocks can be exchanged without excessive overhead of copying data. | low | done | 


## Questions

| Question | Answer |
|----------|--------|


## Out of Scope