# F006: Log

|   |   |
|---|---|
| **Epic** | [GitLab Epic](https://gitlab.com/groups/bern-rtos/kernel/-/epics/6) |
| **Document Status** | active |

## Description

Logs are essential for system and application debugging. They provide information after a failure occurred even if no debug interface was connected.

## Requirements

| ID | Title | User Story | Priority | Status |
|----|-------|------------|----------|--------|
| F006-1 | [Log Sources](https://gitlab.com/bern-rtos/kernel/log/bern-log/-/issues/1) | As a developer, I want to post log messages from any part of the application, so that I can track down software bugs. |  high | done |
| F006-2 | [Log Message](https://gitlab.com/bern-rtos/kernel/log/bern-log/-/issues/2) | As a developer, I want any log message to state its origin, time and severity, so that I can filter irrelevant messages out. | high | done |
| F006-3 | [Log Backends](https://gitlab.com/bern-rtos/kernel/log/bern-log-backend-api/-/issues/1) | As a developer, I want to select the log backends at compile time, so that I can use any hardware interface to output log information. | meidum | done |
| F006-4 | [Log Run Time Filter](https://gitlab.com/bern-rtos/kernel/log/bern-log/-/issues/3) | As a developer, I want to filter log messages per backend at run time, so that I only receive message that are important for me. | low | done |
| F006-5 | [Log Compile Time Filter](https://gitlab.com/bern-rtos/kernel/log/bern-log/-/issues/4) | As a developer, I want to filter log messages at compile time, so that I only the necessary log code is added to flash memory. | medium  | done |
| F006-6 | [RTOS Tracing](https://gitlab.com/bern-rtos/kernel/log/bern-log/-/issues/5) | As a developer, I want the kernel to provide a generic interface for event tracing, so that I can use any software to trace my application. | medium | done |
| F006-7 | [SEGGER SystemView]() | As a developer, I want the tracing backend to support [SEGGER SystemView](https://www.segger.com/products/development-tools/systemview/), so that I can use a simple GUI to analyze my application. | medium | done |


## Questions

| Question | Answer |
|----------|--------|


## Out of Scope