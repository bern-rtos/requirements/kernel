# F002: Synchronization Primitives

|   |   |
|---|---|
| **Epic** | [GitLab Epic](https://gitlab.com/groups/bern-rtos/kernel/-/epics/2) |
| **Document Status** | active |

## Description

Synchronization primitives provide the most basic and essential form of communication between tasks. They protect resources, synchronize work and trigger actions.

## Requirements

| ID | Title | User Story | Priority | Status |
|----|-------|------------|----------|--------|
| F002-1 | [Mutex](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/1) | As a developer, I want that a single resource can only access from one task at a time, so that I can share resources between tasks without data hazards. | high | done | 
| F002-2 | [Priority Inversion](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/2) | As a developer, I want no unbounded priority inversion to occur when a tasks tries to access a shared resource, so that a task doesn't starve. | medium | planned |
| F002-3 | [Counting Semaphore](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/3) | As a developer, I want to create an object that can be taken/given a multiple times, so that I can synchronize multiple tasks. | high | done | 
| F002-4| [Event Flags](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/4) | As a developer, I want to trigger an action on a combination of events, so that I can react to system events efficiently. | low | planned | 

## Questions

| Question | Answer |
|----------|--------|


## Out of Scope