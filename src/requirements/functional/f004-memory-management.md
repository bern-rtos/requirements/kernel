# F004: Memory Management

|   |   |
|---|---|
| **Epic** | [GitLab Epic](https://gitlab.com/groups/bern-rtos/kernel/-/epics/4) |
| **Document Status** | active |

## Description

Volatile and non-volatile memory are critical components for safety and security. This component is responsible for managing access to any memory.

## Requirements

| ID | Title | User Story | Priority | Status |
|----|-------|------------|----------|--------|
| F004-1 | [Stack Overflow](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/16) | As a maintainer, I want to trap stack overflows, so that no other task gets corrupted. | high | done | 
| F004-2 | [Heap](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/24) | As a maker, I want to use Rusts dynamic container types, so that I can use modern programming paradigms. | medium | done | 
| F004-3 | [Memory Pool](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/25) | As a developer, I want to use a deterministic dynamic memory (heap) without fragmentation, so that I don't get fragmentation issues from long run times. | medium | done | 
| F004-4 | [Pool Configuration](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/26) | As a developer, I want to structure the heap at run time, so that I have maximum flexibility in my code. | low | done |
| F004-5 | [Unsafe Isolation](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/17) | As a developer, I want to run unsafe libraries in isolation, so that I can use preexisting code without corruption of my safe application. | low | to review |
| F004-6 | [Kernel Isolation](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/18) | As a maintainer, I want to isolate the kernel memory from user code, so that an error in user code cannot corrupt the whole system. | medium | done |


## Questions

| Question | Answer |
|----------|--------|


## Out of Scope