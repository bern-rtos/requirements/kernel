# F005: Peripheral Management

|   |   |
|---|---|
| **Epic** | [GitLab Epic](https://gitlab.com/groups/bern-rtos/kernel/-/epics/5) |
| **Document Status** | draft |

## Description

Peripherals are hardware components integrated in the MCU which are not part of the CPU core. They drive board components and form an interface to physical world outside the embedded system. This management component assures that access to the peripherals is easy, safe and secure.

## Requirements

| ID | Title | User Story | Priority | Status |
|----|-------|------------|----------|--------|
| F005-1 | [Peripheral Access]() | As a developer, I want to choose which parts of the software are allowed to access peripherals at compile time, so that rogue/malicious task destablizes the embedded system.  | low | to review |
| F005-2 | [Peripheral Sharing]() | As a developer, I want to share bus interfaces amongst multiple tasks, so that separate drivers for hardware components that run on the same bus. | medium | to review |
| F005-3 | [Power Management]() | As a maintainer, I want the RTOS to track the power state of each peripheral, so that the kernel can select sleep modes autonomously. | low | to review |
| F005-4 | [Embedded HAL]() | As a maker, I want to use the [embedded-hal](https://github.com/rust-embedded/embedded-hal), so that I can use existing device drivers. | medium | to review |


## Questions

| Question | Answer |
|----------|--------|


## Out of Scope