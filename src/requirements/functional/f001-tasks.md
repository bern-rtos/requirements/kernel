# F001: Tasks

|   |   |
|---|---|
| **Epic** | [GitLab Epic](https://gitlab.com/groups/bern-rtos/kernel/-/epics/1) |
| **Document Status** | active |

## Description

A task is an entity of work. It is the basis of any RTOS. A task can be run periodically, on a specific event or only once.


## Requirements

| ID | Title | User Story | Priority | Status |
|----|-------|------------|----------|--------|
| F001-1 | [Tasks](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/5) | As a developer, I want to create tasks, so that I can separate independent code sections. | high | done |
| F001-2 | [Task Priorities](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/6) | As a developer, I want to assign priorities to tasks, so that important tasks run on time. | high | done |
| F001-3 | [Task Privileges](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/7) | As a developer, I want to give tasks privileges, so that only certain tasks can access resources. | low | to review |
| F001-4 | [Time Slicing](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/8) | As a developer, I want the CPU to be shared between tasks with equal priority, so that I don't have to bother with switching active tasks. | medium | done |
| F001-5 | [Scheduling Algorithms](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/9) | As a developer, I want to select a scheduling algorithm at compile time, so that configure the RTOS to my needs. | low | to review |
| F001-6 | [Tickless](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/10) | As a developer, I want to disable time slicing in the scheduler, so that I can use the RTOS on ultra low power systems. | low | to review |
| F001-7 | [Async/Await]() | As a developer, I want to use Rusts async/await features, so that I can write code that is easier to understand than with callbacks. | low | to review |
| F001-8 | [Software One-Shot Timer](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/11) | As a developer, I want to schedule an event at some specific time, so that I can defer an action with little overhead. | medium | to review |
| F001-9 | [Software Repetitive Timer](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/12) | As a developer, I want to schedule periodic events in some interval, so that I can trigger short actions with little overhead. | medium | to review |
| F001-10 | [Kernel ISR](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/13) | As a maintainer, I want to handle interrupts in the kernel, so that I can check that context and permissions are correct. | medium | done |
| F001-11 | [User ISR](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/14) | As a developer, I want to register interrupt service routines, so that I can handle interrupt driven events. | medium | planned |
| F001-12 | [Low Latency ISR](https://gitlab.com/bern-rtos/kernel/bern-kernel/-/issues/15) | As a developer, I want to handle time critical events without kernel interference, so that my system can react with minimal latency. | low | to review |

## Questions

| Question | Answer |
|----------|--------|


## Out of Scope
- The worst case execution time (WCET) model will not be covered