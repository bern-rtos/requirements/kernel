# Non-functional Requirements

The non-functional requirements specify how the kernel is supposed to *be*. These quality attribute apply to all components of the system. They have no definition of done, as non-functional requirements apply for every release.

The priority refers to the importance for the project that the requirement is met. All non-functional requirements should be met, but if two are contradicting the one with higher priority will be preferred.