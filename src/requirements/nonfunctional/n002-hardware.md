# N002: Hardware

|   |   |
|---|---|
| **Epic** | |
| **Document Status** | active |

## Description

The amount of memory and safety features depend on the MCU in use. A small memory footprint will allow the kernel to run on any system.


## Requirements

| ID | Title | User Story | Priority |
|----|-------|------------|----------|
| N002-1 | ST Nucleo Support | As a maker, I want to run a preconfigured RTOS on an STM32-Nucleo Board, so that I can start coding my application. | low |
| N002-2 | Flash Requirements | As a developer, I want to kernel with all components enabled to use < 20kB of flash memory, so that the kernel uses <10% of a medium-sized MCU with 256kB flash memory. | medium |
| N002-3 | RAM Requirements | As a developer, I want to kernel with all components enabled to use < 5kB of RAM, so that the kernel uses < 10% of a medium-sized MCU with 56kB RAM. | medium |
| N002-4 | Min. Flash Requirements | As a developer, I want to a minimal version of the kernel to use < 5kB of flash memory, so that the kernel uses < 10% of a small-sized MCU with 64kB flash memory. | low |
| N002-5 | Min. RAM Requirements | As a developer, I want to kernel with all components enabled to use < 2kB of RAM, so that the kernel uses < 10% of a medium-sized MCU with 36kB RAM. | low |
| N002-6 | ARMv7E-M Support | As a company, we want to kernel to support ARMv7E-M (ARM Cortex-M4/M7) MCU with MPU, so that we can choose from many high-performance MCUs and have a stable supply chain. | high |
| N002-7 | ARMv7-M Support | As a company, we want to kernel to support ARMv7-M (ARM Cortex-M3) MCU with MPU, so that we can choose from many medium-performance MCUs and have a stable supply chain. | medium |


## Questions

| Question | Answer |
|----------|--------|


## Out of Scope
- ARMv6-M (ARM Cortex-M0+) support is not of priority for the moment, as its MPU and atomic operation instructions are limited
- ARMv8-M (ARM Cortex-M23/M33) support will be added at some point in the future
- RISC-V support is currently not planned, because they have a small market share