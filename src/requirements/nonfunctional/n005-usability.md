# N005: Usability

|   |   |
|---|---|
| **Epic** | |
| **Document Status** | active |

## Description

The kernel aims to be easy-to-use, so that new users can accommodate to its API fast and to reduce the number bugs overall.

## Requirements

| ID | Title | User Story | Priority | 
|----|-------|------------|----------|
| N005-1 | Multi-Threading Syntax | As a developer, I want to use common Rust multi-threading syntax, so that I can get started easily. | high |
| N005-2 | Kernel Tool Integration | As a developer, I want that kernel tools are integrated into [Cargo](https://doc.rust-lang.org/book/ch14-05-extending-cargo.html), so that I can use a system I'm familiar with. | low |
| N005-3 | Examples | As a maker, I want to build my application on existing examples, so that I have a running application with little effort. | low |

## Questions

| Question | Answer |
|----------|--------|


## Out of Scope