# N001: Modularity

|   |   |
|---|---|
| **Epic** | |
| **Document Status** | active |

## Description

A kernel runs on different platforms. A modular design allows the user to scale the kernel for its application. Modularity also increases flexibility because components can be replaced.


## Requirements

| ID | Title | User Story | Priority |
|----|-------|------------|----------|
| N001-1 | Kernel Dependencies | As a maintainer, I want the dependencies between the kernel components to be kept at a minimum, so that I can rewrite/replace components. | high |
| N001-3 | Application Dependencies | As a company, we want to keep RTOS specific code to a minimum, so we can switch to another RTOS if we need to. | low |
| N001-4 | HAL Interface | As a developer, I want to use my own HAL, so that I can reuse preexisting code. | high |
| N001-5 | Kernel Configuration | As a developer, I want to enable/disable individual kernel components, so that I can tailor the kernel to my needs. | medium |


## Questions

| Question | Answer |
|----------|--------|


## Out of Scope