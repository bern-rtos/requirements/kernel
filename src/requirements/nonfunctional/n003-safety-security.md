# N003: Safety & Security

|   |   |
|---|---|
| **Epic** | |
| **Document Status** | active |


## Description

The kernel safety mechanism provide reliable system performance. Its security mechanisms protect the system from attacks on interfaces.


## Requirements

| ID | Title | User Story | Priority | 
|----|-------|------------|----------|
| N003-1 | Application Security | As a developer, I want that code from an unsafe partition cannot corrupt my application. | high |
| N003-2 | Kernel Security | As a maintainer, I want that no code outside the kernel space can crash the kernel. | high |
| N003-3 | Toolchain | As a maintainer, I want to use Rusts stable toolchain, so that the kernel doesn't rely on unstable language features. | medium |


## Questions

| Question | Answer |
|----------|--------|


## Out of Scope
- [Ferrocene](https://ferrous-systems.com/blog/sealed-rust-the-pitch/) (formerly Sealed Rust) - a Rust subset for safety critical applications - might become a requirement at some point in the future