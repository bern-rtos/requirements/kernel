# N004: Timeliness

|   |   |
|---|---|
| **Epic** | |
| **Document Status** | active |

## Description

Embedded systems react to their environment. They must do so in a timely manner, or the system as a whole might become unstable.

## Requirements

| ID | Title | User Story | Priority | 
|----|-------|------------|----------|
| N004-1 | Task Latency | As a developer, I want high-priority tasks to be called within 1 us after an event occurs (on an ARM Cortex-M4 @ 72 MHz). | medium |
| N004-2 | ISR Latency | As a developer, I want time-critical user ISR to be called within 1 us after the interrupt occurred (on an ARM Cortex-M4 @ 72 MHz). | medium |


## Questions

| Question | Answer |
|----------|--------|


## Out of Scope