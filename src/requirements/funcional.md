# Functional Requirements

The functional requirements specify what the kernel is supposed to *do*. The requirements are grouped by kernel components. Each requirement has a user story and a priority. The acceptance criteria are refined before a sprint in the linked issue. At a later stage test should automatically update the verification status indicator.