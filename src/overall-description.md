# Overall Description

## Product Perspective

The objective of the Bern RTOS project is to implement a safe and secure RTOS for microcontrollers from scratch in the Rust programming language. The core part of the RTOS is the kernel. The figure below shows the kernel components and interfaces to other parts of the system.

![Kernel Architecture](img/architecture.svg)
*Black: kernel components and interfaces, gray: external components*


## Product Functions

The kernel provides features to:

- Schedule tasks
- Synchronize resource accesses and tasks
- Send messages between tasks
- Use dynamic memory (heap)
- Separate safe and unsafe code
- Protect access of peripherals
- Print log messages

## User Personas

- Maintainer: Project maintainer contribute to the Bern RTOS codebase. Their interest is in making embedded systems safe and secure.

- Maker: A hobbyist who wants to write applications quickly without any fuss. The maker expects an RTOS that just works, supports the major platforms and provides many examples. 

- Developer: Developers use the Bern RTOS in products because it provides features that increase the safety and security of the overall system. They expect the kernel to run stable and with minimal overhead. The API documentation must be complete and examples must also cover complex scenarios.

- Company: A company is interested in profit and in a safe/secure system. It will only use the Bern RTOS if safety/security features are built-in and thus can safe time in the development process. A company also expects minimal memory footprint in order for the kernel to run on low-cost hardware.


## Operating Environment

The kernel is intended to run on microcontrollers. In contrast to general purpose operating systems the kernel cannot rely on memory management unit.


## User Documentation

The kernel documentation consists of:

- [User level online documentation](https://kernel.bern-rtos.org/) with details about the kernel internals.
- [API documentation](https://docs.rs/bern-kernel/latest/bern_kernel/) on docs.rs.
- [Code example](https://gitlab.com/bern-rtos/demo) repository.


## Assumptions and Dependencies

### Memory Protection Unit (MPU)

It is assumed that a microcontroller provides a mechanism that allows the kernel to trap access from unprivileged tasks to restricted memory sections. This hardware feature is required for memory partitioning.


### HAL Wrapper

The peripheral manager aims to restrict access to peripherals and shared buses. The manager must work with different HALs. Thus, it is assumed that Rust generics will allow for easy integration of different HAL into the peripheral manager.
